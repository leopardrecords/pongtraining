using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    [Header("Ball")]
    [SerializeField] private GameObject ball;

    [Header("Player1")]
    [SerializeField] private GameObject Player1;
    [SerializeField] private GameObject Player1Goal;

    [Header("Player2")]
    [SerializeField] private GameObject Player2;
    [SerializeField] private GameObject Player2Goal;

    [Header("Score UI")]
    [SerializeField] private GameObject Player1Text;
    [SerializeField] private GameObject Player2Text;

    private int Player1Score;
    private int Player2Score;

    public void Player1Scored()
    {
        Player1Score++;
        Player1Text.GetComponent<TextMeshProUGUI>().text = Player1Score.ToString();
        ResetPosition();
    }

    public void Player2Scored()
    {
        Player2Score++;
        Player2Text.GetComponent<TextMeshProUGUI>().text = Player2Score.ToString();
        ResetPosition();
    }

    private void ResetPosition()
    {
        ball.GetComponent<Ball>().Reset();
        Player1.GetComponent<PlayerMovement>().Reset();
        Player2.GetComponent<PlayerMovement>().Reset();
    }
}
